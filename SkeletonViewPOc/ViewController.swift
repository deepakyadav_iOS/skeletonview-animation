//
//  ViewController.swift
//  SkeletonViewPOc
//
//  Created by Apple on 06/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SkeletonView

class ViewController: UIViewController {
    
    @IBOutlet weak var skeletable_TableView: UITableView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var skeletonTypeSelector: UISegmentedControl!
    @IBOutlet weak var showOrHideSkeletonButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        skeletable_TableView.dataSource = self
        skeletable_TableView.isSkeletonable = true
        avatarImage.isSkeletonable = true
        avatarImage.layer.cornerRadius = avatarImage.frame.width/2
        avatarImage.layer.masksToBounds = true
        showOrHideSkeletonButton.layer.cornerRadius = 10
        showOrHideSkeletonButton.layer.masksToBounds = true
        
        view.showAnimatedSkeleton()
    }

    @IBAction func showOrHideSelection(_ sender: Any) {
        showOrHideSkeletonButton.setTitle((view.isSkeletonActive ? "Show skeleton" : "Hide skeleton"), for: .normal)
               view.isSkeletonActive ? hideSkeleton() : showSkeleton()
    }
    
    @IBAction func changeSkeletonType(_ sender: Any) {
    }
    
    func showSkeleton() {
        view.showAnimatedSkeleton()
    }
    
    func hideSkeleton() {
        view.hideSkeleton()
    }
}


extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "SkeletonableCell", for: indexPath) as! SkeletableTableViewCell
        cell.lblUserName.text = "Deepak Yadav"
        cell.lblMentionName.text = "@perennialSys"
        cell.lblDescription.text  = "Today almost all apps have async processes, such "
    
        return cell
            
    }
    
    
}

