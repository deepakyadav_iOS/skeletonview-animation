//
//  SkeletableTableViewCell.swift
//  SkeletonViewPOc
//
//  Created by Apple on 06/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SkeletableTableViewCell: UITableViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
       
       @IBOutlet weak var lblUserName: UILabel!
       @IBOutlet weak var lblMentionName: UILabel!
       
       @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    imgProfile.layer.cornerRadius = imgProfile.frame.width/2
    imgProfile.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
